import fs from "fs";
import TwitchPrivateMessage from "twitch-chat-client/lib/StandardCommands/TwitchPrivateMessage";
import TwitchClient from "twitch";
import ChatConnection from "./chatClient";

const DEFAULT_DEFAULT_SETTINGS: CommandSettings = {
    atStart: false,
    cooldown: 10,
    includePrefix: true,
    isRegExp: false,
    regExpFlags: "g"
}

const EXTRACT_VARIABLES_REGEXP = /(?<=%)[A-Z]+(?=%)/g;

export type ChatCommand = {
    command: string | RegExp,
    parameters: string[],
    variables: string[],
    possibleAnswers: string[],
    aliases: string[] | RegExp[],
    timeouted: boolean,
    cooldown: number,
    atStart: boolean
}

export type CommandSettings = {
    cooldown?: number,
    atStart?: boolean,
    isRegExp?: boolean,
    regExpFlags?: string,
    includePrefix?: boolean
}

export type CommandVariable = () => PromiseLike<string> | string;
export type CommandAnswerType = (obj: {[key: string] : any, type: string}, command: string) => PromiseLike<string[]> | string[];
export default class CommandHandler {
    chatConnection: ChatConnection;
    protected commands: ChatCommand[] = []
    protected defaultSettings: CommandSettings;
    protected commandVariables: { [key: string]: CommandVariable } = {};
    protected commandAnswerTypes: { [key: string]: CommandAnswerType } = {};

    private constructor(chatConnection: ChatConnection, settings?: CommandSettings) {
        this.chatConnection = chatConnection;
        this.defaultSettings = {
            ...DEFAULT_DEFAULT_SETTINGS,
            ...(settings != null ? settings : {}),
        }

        this.addVariable("uptime", async () => {
            const timeDistance = (date1: Date, date2: Date) => {
                let distance = Math.abs(date1.getTime() - date2.getTime());
                const hours = Math.floor(distance / 3600000);
                distance -= hours * 3600000;
                const minutes = Math.floor(distance / 60000);
                distance -= minutes * 60000;
                const seconds = Math.floor(distance / 1000);
                return `${hours}:${('0' + minutes).slice(-2)}:${('0' + seconds).slice(-2)}`;
            };
            const startDate = await (await this.chatConnection.twitchClient!.helix.streams.getStreamByUserName(this.chatConnection.defaultChannel!))?.startDate;
            return startDate == null ? "---" : timeDistance(new Date(), startDate)
        })
    }

    static fromChatConnection(chatConnection: ChatConnection, settings?: CommandSettings): CommandHandler{
        const commandHandler = new this(chatConnection, settings);
        chatConnection.chatClient!.onPrivmsg(commandHandler.onMessage.bind(commandHandler))
        return commandHandler;
    }

    async addFile(filePath: string): Promise<ChatCommand[]> {
        return await this.addJson(JSON.parse((await fs.promises.readFile(filePath)).toString()));
    }

    async addJson(json: {}): Promise<ChatCommand[]> {
        if (typeof (json as { [key: string]: any })["commands"] != typeof {}) throw new Error("Command object doesn't exists or has an invalid structure");
        const jsonCommands: ChatCommand[] = []

        const prefix: string | undefined = typeof (json as { [key: string]: any })["prefix"] != typeof "" ? undefined : (json as { [key: string]: any })["prefix"];
        await Promise.all(Object.entries((json as { [key: string]: any })["commands"]).map(([key, value]) => (async () => {
            let command: ChatCommand = {
                aliases: [],
                atStart: this.defaultSettings.atStart!,
                command: "",
                cooldown: this.defaultSettings.cooldown!,
                parameters: [],
                possibleAnswers: [],
                timeouted: false,
                variables: []
            }

            let isRegexp: boolean = this.defaultSettings.isRegExp!;
            let includePrefix: boolean = this.defaultSettings.includePrefix!;
            let regExpFlags: string = this.defaultSettings.regExpFlags!;

            const answers = await this.getAnswers(value, key, false, async () => {
                const valueobj = (value as { [key: string]: any });

                const answers = await this.getAnswers(valueobj["possibleAnswers"], key, true) as string[]
                isRegexp = this.extractValues<boolean>(typeof true, valueobj["isRegExp"], this.defaultSettings.isRegExp!);
                includePrefix = this.extractValues<boolean>(typeof true, valueobj["includePrefix"], this.defaultSettings.includePrefix!);
                command.atStart = this.extractValues<boolean>(typeof true, valueobj["atStart"], command.atStart);
                command.cooldown = this.extractValues<number>(typeof 1, valueobj["cooldown"], command.cooldown);
                command.parameters = this.extractValues<string[]>(typeof [""], valueobj["parameters"], command.parameters);
                command.variables = this.extractValues<string[]>(typeof [""], valueobj["variables"], command.variables);
                command.aliases = this.extractValues<string[] | RegExp[]>(typeof [""], valueobj["aliases"], command.aliases);
                regExpFlags = this.extractValues<string>(typeof "", valueobj["regExpFlags"], this.defaultSettings.regExpFlags!);

                if (valueobj["implyVariables"] === true) {
                    let localVariables: string[] = [];
                    answers.forEach((answer: string) => {
                        const matches = answer.match(EXTRACT_VARIABLES_REGEXP)
                        if (matches != null) localVariables = [...localVariables, ...matches];
                    });
                    command.variables = [...new Set([...command.variables, ...localVariables])];
                }
                return answers;
            });
            if (answers === false)
                throw new Error(`Invalid syntax: Value of ${key} needs to be of type string, string[] or {isRegex: boolean = false, includePrefix: boolen = true, parameters?: string[], variables?: string[], possibleAnswers: string | string[], cooldown: number = 10}`);
            command.possibleAnswers = answers as string[]

            command.command = isRegexp ? new RegExp(key, regExpFlags) : includePrefix ? `${prefix}${key}` : key;
            command.aliases = (isRegexp ? (command.aliases as string[]).map((alias: string) => new RegExp(alias, regExpFlags)) : includePrefix ? (command.aliases as string[]).map((alias: string) => `${prefix}${alias}`) : command.aliases)

            jsonCommands.push(command);
        })()));
        this.commands = [...this.commands, ...jsonCommands];
        return jsonCommands;
    }

    protected async onMessage(channel: string, user: string, message: string, msg: TwitchPrivateMessage) {
        if(msg.userInfo.userId == (await this.chatConnection.botTwitchClient!.getTokenInfo()).userId) return;
        const command = this.commands.find((command: ChatCommand) =>
            (!command.timeouted &&
                ((command.command instanceof RegExp &&
                    command.command.exec(message) != null) ||
                    (command.atStart && message.toUpperCase().startsWith((command.command as string).toUpperCase())) ||
                    (message.toUpperCase().includes((command.command as string).toUpperCase())) ||
                    command.aliases.some((alias: string | RegExp) => (
                        (alias instanceof RegExp && alias.exec(message) != null) ||
                        (command.atStart && message.toUpperCase().startsWith((alias as string).toUpperCase())) ||
                        (message.toUpperCase().includes((alias as string).toUpperCase())))))))
        if (command != null)
            this.chatConnection.say(await this.answerMessage(command, channel, user, message, msg), channel);
    }

    protected async answerMessage(command: ChatCommand, channel: string, user: string, message: string, msg: TwitchPrivateMessage): Promise<string> {
        if (command.cooldown != 0) {
            command.timeouted = true;
            setTimeout(() => {
                command.timeouted = false;
            }, command.cooldown * 1000);
        }

        this.addVariable("user", () => user);

        const variables: { [key: string]: string } = {};
        await Promise.all(command.variables.map((variable: string) => (async () => {
            const valueFunction = this.commandVariables[variable.toUpperCase()];
            if (valueFunction == undefined)
                throw new Error(`Error: Variable "${variable.toUpperCase()}" not found`);
            variables[variable.toUpperCase()] = await valueFunction();
        })()));

        let choosenAnswer: string = command.possibleAnswers[Math.floor(Math.random() * command.possibleAnswers.length)];
        Object.entries(variables).forEach(([key, value]) => {
            choosenAnswer = choosenAnswer.split(`%${(key as string).toUpperCase()}%`).join(value);
        })
        return choosenAnswer;
    }

    protected async getAnswers(answerObj: any, command: string, throwIfTypeNotFound: boolean = false, objectCallback?: () => void | string[] | PromiseLike<void | string[]>): Promise<string[] | boolean> {
        let answers: string[] | undefined = undefined
        if (answerObj == null ||
            !((answerObj instanceof Array &&
                answerObj.every((val: any) => typeof val == typeof "")) ||
                typeof answerObj == typeof "" || (typeof answerObj == typeof {} && (objectCallback != null || (Object.keys(answerObj).every(key => typeof key == typeof "") && typeof answerObj["type"] == typeof "")))))
            throw new Error(`Invalid syntax: answers of ${command} needs to be of type string or string[]`);
        if (typeof answerObj == typeof "") answers = [answerObj as string];
        else if (answerObj instanceof Array) answers = answerObj as string[];
        else if (typeof answerObj == typeof {}) {
            if (answerObj["type"] == undefined) answerObj["type"] = "";
            const answerFunction = this.commandAnswerTypes[answerObj["type"].toUpperCase()];
            if(answerFunction == undefined) {
                if (objectCallback != null) { const cbAnswers = await objectCallback(); if (cbAnswers != null) answers = cbAnswers }
                else throw new Error(`answers type ${answerObj["type"]} isn't implemented (command: ${command})`);
            } else {
                answers = await answerFunction(answerObj, command);
            }
        }
        else if (throwIfTypeNotFound) throw new Error(`answers of ${command} needs to be of type string, string[] or {type: string, [key: string]: any}`);
        return (answers != null ? answers : false);
    }

    protected extractValues<T>(type: string, value: T | undefined, defaultValue: T, extendArray = true): T {
        const val = (value != null && typeof value == type ? value : defaultValue) as T;
        if (extendArray && val instanceof Array) {
            return [...(new Set([...(value != undefined ? val : []), ...val]))] as unknown as T;
        }
        return val;
    }

    addVariable(name: string, valueCB: CommandVariable) {
        this.commandVariables[name.toUpperCase()] = valueCB;
    }

    addVariables(variables: { [key: string]: CommandVariable }) {
        Object.entries(variables).forEach(([variable, valueCB]) => {
            this.addVariable(variable, valueCB);
        })
    }
    
    addAnswerType(name: string, valueCB: CommandAnswerType) {
        this.commandAnswerTypes[name.toUpperCase()] = valueCB;
    }

    addAnswerTypes(variables: { [key: string]: CommandAnswerType }){
        Object.entries(variables).forEach(([variable, valueCB]) => {
            this.addAnswerType(variable, valueCB);
        })
    }
}