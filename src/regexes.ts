import axios from "axios";

export default async () => {
    const TLDListResponse = await axios.get("http://data.iana.org/TLD/tlds-alpha-by-domain.txt");
    if(TLDListResponse.status != 200)
        throw new Error(`Failed to fetch TLDs: ${TLDListResponse.statusText}`);
    const TLDArray = TLDListResponse.data.split("\n").map((line: string) => line.trim()).filter((line: string) => !line.startsWith("#") && !(line ==""));
    return {
        URLDetectorRegEx: new RegExp(`[^\\s.]+?\\.(?:${TLDArray.join("|")})`, "gi"),
        TwitchClipsURLDetectorRegEx: /(?<=https:\/\/| http:\/\/)clips.twitch.tv\/\S+/gi
    }
}