import TwitchClient from "twitch";
import { PubSubRedemptionMessage } from "twitch-pubsub-client";
import fs from "fs";
import RandomAnswers from "./answers";
import PubSubConnection from "./pubSubConnection";
import RegexResolvable from "./regexes";
import ChatConnection from "./chatClient";
import PuppeteerPoll from "puppeteerpoll";
import CommandHandler from "./commandHandler";

const devMode: boolean = process.env["DEVELOPMENT"] === "true";
const disablePuppeteer = devMode;

(async () => {
    const { URLDetectorRegEx, TwitchClipsURLDetectorRegEx } = await RegexResolvable();

    let tokensJson = JSON.parse(await fs.promises.readFile("./tokens.json", { encoding: "utf8" }));

    const clientId: string = tokensJson["clientId"] || "";
    const clientSecret: string = tokensJson["clientSecret"] || "";
    const accessToken: string = tokensJson["accessToken"];
    const refreshToken: string = tokensJson["refreshToken"] || "";
    const botAccountAccessToken: string = tokensJson["botAccountAccessToken"];
    const botAccountRefreshToken: string = tokensJson["botAccountRefreshToken"] || "";
    const channelName: string = tokensJson["channelName"] || "";
    const emailUsername: string = tokensJson["botEmailAccountUsername"] || "";
    const emailPassword: string = tokensJson["botEmailAccountPassword"] || "";
    const botAccountUsername: string = tokensJson["botAccountUsername"] || "";
    const botAccountPassword: string = tokensJson["botAccountPassword"] || "";
    const imapServer: string = tokensJson["imapServer"] || "";

    let expiryTimestamp: string | undefined = tokensJson["expiryTimestamp"] == null ? undefined : tokensJson["expiryTimestamp"];
    let botAccountExpiryTimestamp: string | undefined = tokensJson["botAccountExpiryTimestamp"] == null ? undefined : tokensJson["botAccountExpiryTimestamp"];

    if (clientId.trim() == "" || channelName.trim() == "" || clientSecret.trim() == "" || refreshToken.trim() == "" || botAccountRefreshToken.trim() == "") {
        console.log({ clientId, channelName, clientSecret, refreshToken, botAccountRefreshToken });
        throw new Error("Please fill in clientId clientSecret accessToken refreshToken and channelName in tokens.json");
    }

    const puppeteerPoll = await PuppeteerPoll.initTwitchPage({ emailPassword, emailUsername, username: botAccountUsername, password: botAccountPassword, imapServer, debug: true, developmentMode: disablePuppeteer });

    const client = TwitchClient.withCredentials(clientId, accessToken, undefined, {
        clientSecret,
        refreshToken,
        expiry: expiryTimestamp == undefined ? null : new Date(expiryTimestamp),
        onRefresh: async ({ accessToken, refreshToken, expiryDate }) => {
            const newTokenData = {
                ...tokensJson,
                accessToken,
                refreshToken,
                expiryTimestamp: expiryDate == null ? null : expiryDate.getTime()
            }
            tokensJson = newTokenData;
            await fs.promises.writeFile(`./tokens.json`, JSON.stringify(newTokenData, null, 4), "utf8");
        }
    });

    const botAccClient = TwitchClient.withCredentials(clientId, botAccountAccessToken, undefined, {
        clientSecret: clientSecret,
        refreshToken: botAccountRefreshToken,
        expiry: botAccountExpiryTimestamp == undefined ? null : new Date(botAccountExpiryTimestamp),
        onRefresh: async ({ accessToken, refreshToken, expiryDate }) => {
            console.log("hi");
            const newTokenData = {
                ...tokensJson,
                botAccountAccessToken: accessToken,
                botAccountRefreshToken: refreshToken,
                botAccountExpiryTimestamp: expiryDate == null ? null : expiryDate.getTime()
            }
            tokensJson = newTokenData;
            await fs.promises.writeFile(`./tokens.json`, JSON.stringify(newTokenData, null, 4), "utf8");
        }
    });

    const user = await client.helix.users.getUserByName(channelName);

    const randomAnswers = await RandomAnswers.fromFile("./answers.json", { channelname: user?.name });
    if (user == null) throw new Error("Error: User not  found");

    const pubSubConnection = await PubSubConnection.fromTwitchClient(client, user);
    const chatConnection = await ChatConnection.fromTwitchClient(client, user.name, randomAnswers, undefined, botAccClient);
    const commandHandler = CommandHandler.fromChatConnection(chatConnection);
    commandHandler.addAnswerType("randomAnswers", (answerObj, command) => {
        if (typeof answerObj["id"] != typeof "") throw new Error(`The field id of the answers of ${command} for the type \"randomAnswers\" needs to be a string`);
        if (randomAnswers.answers[answerObj["id"]] == null) throw new Error(`No answers found for randomAnswers id ${answerObj["id"]} (command: ${command})`)
        return randomAnswers.answers[answerObj["id"]];
    })
    await commandHandler.addFile("./commands.json")

    console.log("started");

    pubSubConnection.addSpecifiedRedemptionListener(["Test", "Eine Umfrage vorschlagen"], async (message: PubSubRedemptionMessage) => {
        const questions = message.message.match(/^.+?(?=[0-5][).]\s?|$)/g);
        const answers = message.message.match(/(?<=[0-5][).]\s?).+?(?=[0-5][).]\s?|$)/g)?.map((answer) => answer.trim());

        if (questions == null || questions.length != 1 || answers == null || answers.length < 2 || answers.length > 5) {
            await chatConnection.sayRandomAnswer("invalidPollRequest", { user: message.userName });
            return;
        }
        const answersVariables = { user: message.userDisplayName, question: questions[0].toString(), answers: answers.join("; ") };

        await Promise.all([(async () => {
           // Verify 
        })(),
        await chatConnection.sayRandomAnswer("redeemedPoll", answersVariables)
        ])


        await puppeteerPoll.startPoll(questions[0], answers, "1MIN", undefined, 1000);
    })


    chatConnection.chatClient!.onAction(async (_, user, __, msg) => {
        if (!msg.userInfo.isMod && !msg.tags.has("broadcaster") && !msg.tags.has("verified")) await Promise.all([chatConnection.deleteMessage(msg), chatConnection.sayRandomAnswer("blockAction", { user })])
    })

    chatConnection.chatClient!.onPrivmsg(async (_, user, message, msg) => {
        const urlsInMessage: number = ((match: RegExpMatchArray | null) => match == null ? 0 : match.length)(message.match(URLDetectorRegEx));
        const twitchClipsInMessage: number = ((match: RegExpMatchArray | null) => match == null ? 0 : match.length * 2)(message.match(TwitchClipsURLDetectorRegEx));
        if (urlsInMessage - twitchClipsInMessage >= 1 && !msg.userInfo.isMod && !msg.tags.has("broadcaster") && !msg.tags.has("verified")) await Promise.all([chatConnection.deleteMessage(msg), chatConnection.sayRandomAnswer("blockLink", { user })]);
    })

})();