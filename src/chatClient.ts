import ChatClient, { PrivateMessage } from "twitch-chat-client/lib";
import { ChatClientOptions } from "twitch-chat-client/lib/ChatClient";
import TwitchClient from "twitch";
import RandomAnswers from "./answers";
import CommandHandler from "./commandHandler";

export default class ChatConnection {
    defaultChannel?: string;
    chatClient?: ChatClient;
    botClient?: ChatClient;
    twitchClient?: TwitchClient;
    botTwitchClient?: TwitchClient;
    randomAnswers?: RandomAnswers;
    protected constructor() { }

    static async fromTwitchClient(twitchClient: TwitchClient, defaultChannel?: string, randomAnswers?: RandomAnswers, options: ChatClientOptions = {}, botClient?: TwitchClient, botOptions: ChatClientOptions = {}, ): Promise<ChatConnection> {
        const chatConnection = new ChatConnection();
        chatConnection.twitchClient = twitchClient;
        chatConnection.botTwitchClient = botClient == null? twitchClient: botClient;
        chatConnection.chatClient = ChatClient.forTwitchClient(twitchClient, { ...options, channels: defaultChannel != null ? [defaultChannel!] : options?.channels });
        if (botClient != null) chatConnection.botClient = ChatClient.forTwitchClient(botClient!, { ...botOptions, channels: defaultChannel != null ? [defaultChannel!] : botOptions?.channels });
        chatConnection.defaultChannel = defaultChannel;
        chatConnection.randomAnswers = randomAnswers;
        await chatConnection.chatClient.connect();
        if (chatConnection.botClient != null) await chatConnection.botClient.connect();
        if (botClient == null) chatConnection.botClient = chatConnection.chatClient;
        return chatConnection;
    }

    async say(message: string, channel?: string) {
        if (this.defaultChannel == null && channel == null) throw new Error("No channel specified!");
        await this.botClient!.say((channel != null ? channel : this.defaultChannel)!, message);
    }

    async sayRandomAnswer(id: string, variables?: {}, channel?: string, merge: boolean = true) {
        if (this.randomAnswers == null) throw new Error("ChatConnection.sayRandomAnswer isn't usable without an random answer handler / instance. Provide your RandomAnswer instance inside of the Constructor.");
        if (this.defaultChannel == null && channel == null) throw new Error("No channel specified!");
        await this.say(this.randomAnswers.getMessage(id, variables, merge), channel)
    }

    async deleteMessage(msg: string | PrivateMessage, channel?: string) {
        if (this.defaultChannel == null && channel == null) throw new Error("No channel specified!");
        await this.chatClient!.deleteMessage((this.defaultChannel == null ? channel : this.defaultChannel)!, msg);
    }
}