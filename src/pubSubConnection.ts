import TwitchClient, { User, HelixUser } from "twitch";
import PubSubClient, { PubSubRedemptionMessage } from "twitch-pubsub-client";

export class PubSubListenerId {
    id: number;
    type: string;
    constructor(id: number, type: string) {
        this.id = id;
        this.type = type;
    }
}

export default class PubSubConnection {
    [x: string]: any;
    twitchClient: TwitchClient;
    pubSubClient: PubSubClient;
    listeners: Map<String, Array<Function | undefined>> = new Map();
    protected constructor(twitchClient: TwitchClient) {
        this.twitchClient = twitchClient;
        this.pubSubClient = new PubSubClient();
    }
    static async fromTwitchClient(twitchClient: TwitchClient, user: HelixUser | User): Promise<PubSubConnection> {
        const pubSubClient = new PubSubConnection(twitchClient);
        await pubSubClient.pubSubClient.registerUserListener(twitchClient, user);

        const pubSubPrototype = Reflect.getPrototypeOf(pubSubClient.pubSubClient);

        Object.keys(pubSubPrototype).filter((method) => method.startsWith("on")).forEach((method) => {
            Reflect.defineProperty(pubSubClient, `add${((marr) => { marr.splice(0, 2); return marr })(method.split("")).join("")}Listener`, {
                value: (fn: (message: any) => void) => {
                    if (!pubSubClient.listeners.has(method)) {
                        pubSubClient.listeners.set(method, []);
                        let listenerF = (msg: any) => {
                            pubSubClient.listeners.get(method)?.forEach((listener) => {
                                if (listener != null) listener(msg);
                            });
                        }
                        eval(`try{pubSubClient.pubSubClient.${method}(user,listenerF)}catch(e){}`);
                    }
                    pubSubClient.listeners.get(method)?.push(fn);
                    return new PubSubListenerId(pubSubClient.listeners.get(method)!.length, method);
                },
                writable: false
            });
        })

        return pubSubClient;
    }

    removeListener(id: PubSubListenerId) {
        if(this.listeners.has(id.type) && this.listeners.get(id.type)![id.id] != null){
            this.listeners.get(id.type)![id.id] = undefined;
        }
    }

    addSpecifiedRedemptionListener(name: String | String[], fn : (message: PubSubRedemptionMessage, channelName? : String) => void, options?: {queued?: boolean, status?: "FULFILLELD" | "UNFULFILLED"}){
        this.addRedemptionListener(async (message: PubSubRedemptionMessage) => {
            const channelName = (await message.getChannel())?.name;

            if(((name instanceof String && message.rewardName == name) || ( typeof name == typeof [""] && name.includes(message.rewardName)))
                && (options?.queued == null || message.rewardIsQueued == options!.queued!)
                && (options?.status == null || message.status == options!.status!) ){
                    fn(message, channelName);
            }
        });
    }

}